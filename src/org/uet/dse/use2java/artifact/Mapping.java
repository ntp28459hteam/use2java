/**
 * 
 */
package org.uet.dse.use2java.artifact;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.stream.Stream;

import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.ocl.type.Type;

import org.tzi.use.uml.mm.MClass;

/**
 * @author Admin
 *
 */
public class Mapping {
	private MModel model;
	
	public Mapping(MModel model) {
		this.model = model;
		toJavaFiles();
	}
	
	private void toJavaFiles() {
		Collection<MClass> classes = model.classes();
		String basePath = new File("").getAbsolutePath();
	    System.out.println(basePath);
		for(MClass clazz : classes) {
//			String path = new File("resources/" + clazz.name() + ".java")
//		    		.getAbsolutePath();
//			System.out.println(path);
			File file = new File("I:\\Users\\Admin\\Desktop\\resources\\" + clazz.name() + ".java");
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			toJavaFile(clazz, file);
		}
	}
	
	private void toJavaFile(MClass clazz, File dst) {
		Collection<MAttribute> attrs = clazz.allAttributes();
		Collection<MOperation> ops = clazz.allOperations();
		Collection<MAssociation> ass = clazz.allAssociations();
		String content = String.format("public class %s {\n", clazz.name());
		
		for(MAttribute attr : attrs) {
			content += String.format("\tprivate %s %s;\n", attr.type(), attr.name());
		}
		
		for(MOperation op : ops) {
			content += String.format("\tpublic %s %s(%s) {\n\t\t\n\t}\n", op.resultType(), op.name(), op.paramList());
		}
		
		content += "}";
		
		FileWriter fWriter;
		try {
			fWriter = new FileWriter(dst);
			BufferedWriter buf = new BufferedWriter(fWriter);
			buf.write(content);
			buf.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
}
