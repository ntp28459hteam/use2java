/**
 * 
 */
package org.uet.dse.use2java.gui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Paths;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

import org.tzi.use.config.Options;
import org.tzi.use.gui.main.MainWindow;
import org.tzi.use.gui.util.CloseOnEscapeKeyListener;
import org.tzi.use.gui.util.ExtFileFilter;
import org.tzi.use.gui.util.GridBagHelper;
import org.tzi.use.main.ChangeEvent;
import org.tzi.use.main.ChangeListener;
import org.tzi.use.main.Session;
import org.uet.dse.use2java.USE2JavaLoader;

/**
 * @author Admin
 *
 */
public class USE2JavaParserParameter extends JDialog {
	private Session fSession;
	private MainWindow mainWindow;
	private PrintWriter logWriter;
	private JTextField fTextModel2;
	private JTextField fTextTgg;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public USE2JavaParserParameter(Session session, MainWindow parent) {
		super(parent, "USE2Java Parser Parameter");
		mainWindow = parent;
		fSession = session;
		fSession.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				closeDialog();
			}
		});

		logWriter = parent.logWriter();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		fTextModel2 = new JTextField(35);
		
		JButton btnParse = new JButton("Parse");
		btnParse.setMnemonic('P');
		btnParse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parseUSE();
			}
		});
		
		JButton btnPath2 = new JButton("Browse...");
		btnPath2.addActionListener(new ActionListener() {
			private JFileChooser fChooser;

			public void actionPerformed(ActionEvent e) {
				String path;
				if (fChooser == null) {
					path = Options.getLastDirectory().toString();
					fChooser = new JFileChooser(path);
					ExtFileFilter filter = new ExtFileFilter("use", "USE specifications");
					fChooser.setFileFilter(filter);
					fChooser.setDialogTitle("Open specification");
				}
				int returnVal = fChooser.showOpenDialog(USE2JavaParserParameter.this);
				if (returnVal != JFileChooser.APPROVE_OPTION)
					return;

				path = fChooser.getCurrentDirectory().toString();
				Options.setLastDirectory(new File(path).toPath());
				fTextModel2.setText(Paths.get(path, fChooser.getSelectedFile().getName()).toString());
			}
		});
		
		JComponent contentPane = (JComponent) getContentPane();
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		GridBagHelper gh = new GridBagHelper(contentPane);
		gh.add(fTextModel2, 0, 2, 6, 1, 0.0, 0.0, GridBagConstraints.HORIZONTAL);
		gh.add(btnPath2, 0, 1, 6, 1, 0.0, 0.0, GridBagConstraints.HORIZONTAL);
		gh.add(btnParse, 0, 0, 6, 1, 0.0, 0.0, GridBagConstraints.HORIZONTAL);
		
		
		getRootPane().setDefaultButton(btnParse);
		pack();
		setLocationRelativeTo(parent);

		// Close dialog on escape key
		CloseOnEscapeKeyListener ekl = new CloseOnEscapeKeyListener(this);
		addKeyListener(ekl);
	}
	
	private void closeDialog() {
		setVisible(false);
		dispose();
	}
	
	private boolean checkPath() {
		File f = new File(fTextModel2.getText());
		if (f.exists()) {
			f = new File(fTextTgg.getText());
			if (f.exists())
				return true;
		}
		return false;
	}
	
	private void parseUSE() {
		USE2JavaLoader loader = new USE2JavaLoader(fSession, new File(fTextModel2.getText()), logWriter);
		loader.run();
	}

}
