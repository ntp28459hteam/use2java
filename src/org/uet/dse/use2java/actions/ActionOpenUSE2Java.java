/**
 * 
 */
package org.uet.dse.use2java.actions;

import org.tzi.use.gui.main.MainWindow;
import org.tzi.use.main.Session;
import org.tzi.use.runtime.gui.IPluginAction;
import org.tzi.use.runtime.gui.IPluginActionDelegate;
import org.uet.dse.use2java.gui.USE2JavaParserParameter;

/**
 * @author Admin
 *
 */
public class ActionOpenUSE2Java implements IPluginActionDelegate {

	@Override
	public void performAction(IPluginAction pluginAction) {
		Session fSession = pluginAction.getSession();
		MainWindow fMainWindow = pluginAction.getParent();
		USE2JavaParserParameter  fParamForm = new USE2JavaParserParameter(fSession, fMainWindow);
		fParamForm.setResizable(true);
        fParamForm.setVisible(true);
	}

}
