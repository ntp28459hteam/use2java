/**
 * 
 */
package org.uet.dse.use2java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.tzi.use.api.UseApiException;
import org.tzi.use.api.UseSystemApi;
import org.tzi.use.main.Session;
import org.tzi.use.parser.use.USECompiler;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.ModelFactory;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.sys.MSystem;
import org.uet.dse.use2java.artifact.Mapping;

/**
 * @author Admin
 *
 */
public class USE2JavaLoader {
	private Session session;
	private File modelFile;
	private PrintWriter logWriter;
	
	
	
	private MModel mergedModel;
	private MModel diagramModel;
	private String modelName;
	private StringBuilder useContent = new StringBuilder();
	
	private MModel model;
	
	private List<MClass> mClasses = new ArrayList<>();
	private List<MAssociation> mAss = new ArrayList<>();
	private List<MObject> mObjects = new ArrayList<>();
	private List<MAttribute> mAttr = new ArrayList<>();
	private List<MOperation> mOp = new ArrayList<>();
	
	
	public USE2JavaLoader(Session session, File modelFile, PrintWriter logWriter) {
		super();
		this.session = session;
		this.modelFile = modelFile;
		this.logWriter = logWriter;
	}
	public void run() {
		parseModels();
		MModel newModel = null;
		MSystem system = null;
		try {
			newModel = USECompiler.compileSpecification(useContent.toString(), modelName, logWriter,
					new ModelFactory());
			logWriter.println("Load model " + modelName + " ...");
			if (newModel != null) {
				logWriter.println(newModel.getStats());
				// create system
				system = new MSystem(newModel);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// set new system (may be null if compilation failed)
		final MSystem system2 = system;
		session.setSystem(system2);
		if(session.hasSystem()) {
			MModel model = session.system().model();
			Iterable<MClass> classes = model.classes();
			for(MClass clazz : classes) {
				System.out.println(clazz.name());
			}
		}
		logWriter.println("Compilation successful");
		
		MModel model = session.system().model();
		Mapping map = new Mapping(model);
	}
	
	private void parseModels() {
		String model = unionModel();
		useContent.append(model);
		mergedModel = USECompiler.compileSpecification(model, modelName, logWriter, new ModelFactory());
		diagramModel = USECompiler.compileSpecification(model.replace("abstract class ", "class "), modelName, logWriter, new ModelFactory());
	}
	
	private String unionModel() {
		String result = "";
		FileInputStream stream;
		try {
			String mm1 = "", mm2 = "";
			stream = new FileInputStream(modelFile);
			model = USECompiler.compileSpecification(stream, modelFile.getName(), logWriter, new ModelFactory());
			byte[] bytes = Files.readAllBytes(modelFile.toPath());
			mm1 = new String(bytes, "UTF-8");
			mm1 = mm1.substring(mm1.indexOf('\n', mm1.indexOf("model")));

			
//			stream = new FileInputStream(trgModelFile);
//			model2 = USECompiler.compileSpecification(stream, trgModelFile.getName(), logWriter, new ModelFactory());
//			bytes = Files.readAllBytes(trgModelFile.toPath());
//			mm2 = new String(bytes, "UTF-8");
//			mm2 = mm2.substring(mm2.indexOf('\n', mm2.indexOf(RTLKeyword.model)));
			modelName = model.name() + "USE2Java";
			// All model
			result = "model" + " " + modelName + "\n" + mm1 + "\n" + mm2;
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
