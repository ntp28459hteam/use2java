/**
 * 
 */
package org.uet.dse.use2java.gui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.tzi.use.gui.main.MainWindow;

/**
 * @author Admin
 *
 */
@SuppressWarnings("serial")
public class Content extends JPanel {
	private final MainWindow fMainWindow;
	
	public Content(MainWindow mainWindow) {
		this.fMainWindow = mainWindow;
		setLayout(new BorderLayout());

		this.setFocusable(true);
		JPanel content = new JPanel(new BorderLayout());
		JLabel label = new JLabel("This is a jpanel content");
		content.add(label);
		add(new JScrollPane(content));
	}
}
