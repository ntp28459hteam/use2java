/**
 * 
 */
package org.uet.dse.use2java;

import org.tzi.use.runtime.IPlugin;
import org.tzi.use.runtime.IPluginRuntime;
import org.tzi.use.uml.sys.MSystem;

/**
 * @author Admin
 *
 */
public class Main implements IPlugin {

	@Override
	public String getName() {
		return "Plugin name: uml2java";
	}


	@Override
	public void run(IPluginRuntime pluginRuntime) throws Exception {
	}

}
