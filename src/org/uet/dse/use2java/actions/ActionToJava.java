package org.uet.dse.use2java.actions;

import java.beans.PropertyVetoException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.tzi.use.gui.main.MainWindow;
import org.tzi.use.gui.main.ViewFrame;
import org.tzi.use.main.Session;
import org.tzi.use.runtime.gui.IPluginAction;
import org.tzi.use.runtime.gui.IPluginActionDelegate;
import org.uet.dse.use2java.Main;
import org.uet.dse.use2java.gui.Content;

public class ActionToJava implements IPluginActionDelegate {

	@Override
	public void performAction(IPluginAction pluginAction) {
		MainWindow mainWindow = pluginAction.getParent();
		Session curentSession = pluginAction.getSession();
		
		URL url = Main.class.getResource("/resources/diagram.png");
		
		
		
		ViewFrame vf = new ViewFrame("Hello, USE viewframe", null, "");
		vf.setFrameIcon(new ImageIcon(url));
		
		vf.addInternalFrameListener(new InternalFrameListener() {
			@Override
			public void internalFrameActivated(InternalFrameEvent arg0) {
			}
			@Override
			public void internalFrameClosed(InternalFrameEvent arg0) {					
			}
			@Override
			public void internalFrameClosing(InternalFrameEvent arg0) {					
			}
			@Override
			public void internalFrameDeactivated(InternalFrameEvent arg0) {					
			}
			@Override
			public void internalFrameDeiconified(InternalFrameEvent arg0) {					
			}
			@Override
			public void internalFrameIconified(InternalFrameEvent arg0) {					
			}
			@Override
			public void internalFrameOpened(InternalFrameEvent arg0) {
				try {
					arg0.getInternalFrame().setMaximum(true);
				} catch (PropertyVetoException e) {
				} 					
			}
			
		});
		
		vf.setContentPane(new Content(mainWindow));
		vf.pack();
		mainWindow.addNewViewFrame(vf);
	}

}
